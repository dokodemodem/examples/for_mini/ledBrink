/*
 * Sample program for DokodeModem
 * led brink
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void setup()
{
  Dm.begin(); // 初期化が必要です。
}

void loop()
{
  Dm.LedCtrl(RED_LED, ON);
  Dm.LedCtrl(GREEN_LED, ON);
  delay(500);
  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);
  delay(500);
}
